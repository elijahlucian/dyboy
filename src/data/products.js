export default [
  {
    name: 'Pinup Mini Session',
    price: 150,
    expenses: [
      // name from type
      // polaroid
      { name: 'partner cut', percentage: 20 },
      { name: 'gas', cost: 44 * 0.2 },
      { name: 'hmua', cost: 0 },
    ],
    timeBlocks: [
      // name from type
      { name: 'editing', hours: 4 },
      { name: 'client acquisition', hours: 2 },
      { name: 'invoicing', hours: 0.5 },
      { name: 'image selection', hours: 1 },
    ],
  },
  {
    name: 'Pinup Photo Package',
    price: 300,
    expenses: [
      // name from type
      { name: 'partner cut', percentage: 20 },
      { name: 'hmua', cost: 50 },
    ],
    timeBlocks: [
      // name from type
      { name: 'editing', hours: 4 },
      { name: 'client acquisition', hours: 1 },
      { name: 'invoicing', hours: 0.5 },
    ],
  },
]
