interface Vendor {
  name: string
  rate: number
}

interface TimeBlockType {
  // this could be used in the future to hep with outsourcing
  // this could alos be used to create schedules of things!
  name: string
}

interface ExpenseType {
  // fixed cost, price per unit, etc
  name: string
}

interface ProductType {
  // physical, digital asset, service
  name: string
}

interface Timeblock {
  type: TimeBlockType
  hours: number
  oursourceRate: number
}

interface Expense {
  type: ExpenseType
  cost: number
  vendor: Vendor
}

interface Product {
  type: ProductType
  name: string
  price: number
  expenses: Array<Expense>
  time_blocks: Array<Timeblock>
}
